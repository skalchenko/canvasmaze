pc.script.create('player_input', function(app) {
    var PlayerInput = function (entity) {
        this.entity = entity;

        this.x = new pc.Vec3();
        this.z = new pc.Vec3();
        this.heading = new pc.Vec3();

        // var element = document.body;
        this.controller = new pc.input.Controller(window);

        this.controller.registerKeys('forward', [pc.input.KEY_UP, pc.input.KEY_W]);
        this.controller.registerKeys('back', [pc.input.KEY_DOWN, pc.input.KEY_S]);
        this.controller.registerKeys('left', [pc.input.KEY_LEFT, pc.input.KEY_A]);
        this.controller.registerKeys('right', [pc.input.KEY_RIGHT, pc.input.KEY_D]);
        this.controller.registerKeys('jump', [pc.input.KEY_SPACE]);
    };

    PlayerInput.prototype = {
        initialize: function () {
//            this.playing = this.playing;
            this.audio = app.root.findByName('Steps');
            this.camera = app.root.findByName('player_camera');
            this.player = this.entity;
            this.characterController = 'dynamic_character_controller';
            this.footstepsTimer = 0;
            app.keyboard.on(pc.input.EVENT_KEYDOWN, this.onKeyDown, this);
        },

        //prevents default browser actions, such as scrolling when pressing cursor keys
        onKeyDown: function (event) {
            event.event.preventDefault();
        },

        update: function (dt) {
            var input = false;

            this.footstepsTimer += dt;

            // Calculate the camera's heading in the XZ plane
            var transform = this.camera.getWorldTransform();

            transform.getZ(this.z);
            this.z.y = 0;
            this.z.normalize();

            transform.getX(this.x);
            this.x.y = 0;
            this.x.normalize();

            this.heading.set(0, 0, 0);

            // Strafe left/right
            if (this.controller.isPressed('left')) {
                this.heading.sub(this.x);
                input = true;
            } else if (this.controller.isPressed('right')) {
                this.heading.add(this.x);
                input = true;
            }

            // Move forwards/backwards
            if (this.controller.isPressed('forward')) {
                this.heading.sub(this.z);
                input = true;
            } else if (this.controller.isPressed('back')) {
                this.heading.add(this.z);
                input = true;
            }

            if (this.controller.wasPressed('back') || this.controller.wasPressed('forward') || this.controller.wasPressed('right') || this.controller.wasPressed('left')) {
                if (this.playing === false) {
                    this.playing = true;
                }
            } else {
                this.playing = false;
            }

            if (input) {
                this.heading.normalize();
            }

            if (this.playing === true && this.footstepsTimer > 0.5){
                this.audio.audiosource.play("166508__yoyodaman234__concrete-footstep-2.wav");
                this.playing = false;
                this.footstepsTimer = 0;
            }

            this.player.script.player_dynamic.move(this.heading);

            if (this.controller.wasPressed('jump')) {
                this.player.script.player_dynamic.jump();
            }

            this.playing = false;
        }
    };

    return PlayerInput;
});
