(function(name) {
    if (!pc.Application.getApplication().scripts.has(name)) {
        var PlayerVrUi = pc.createScript(name);

        PlayerVrUi.attributes.add("camera", {type: "entity", title: "Camera"});
        PlayerVrUi.attributes.add("enterVrWhite", {type: "string", title: "Enter VR White url"});
        PlayerVrUi.attributes.add("enterVrOrange", {type: "string", title: "Enter VR Orange url"});
        PlayerVrUi.attributes.add("infoBoxLifeSpan", {type: "number", default: 3, title: "Info Box Life Span"});

        PlayerVrUi.prototype.initialize = function() {
            if (this.app.vr && this.app.vr.display) {
                this.app.vr.display.on("presentchange", this.onVrPresentChange, this);
            }

            // HTML UI setup
            var css = '#vr-button {position: absolute;right: 0px;bottom: 0px;background-image: url("'+ this.enterVrWhite +'");width: 146px;height: 104px;display: block;'+
                'background-position: 0px 0px;background-size: 146px 104px; cursor: pointer;}' +
                '#vr-button:hover {background-image: url("' + this.enterVrOrange + '");}' +
                '#info-box {position: absolute;	right: 140px;bottom: 26px;display: block;background-color: rgba(0,0,0, 168);color: rgb(218, 218, 218);padding: 5px 10px 5px 10px;max-width: 220px;}' +
                '#info-box a, #info-box a:hover, #info-box a:visited, #info-box a:active {text-decoration: underline;color: rgb(218, 218, 218);}';

            var style = pc.createStyle(css);
            document.head.appendChild(style);

            this.vrButtonDiv = document.createElement("div");
            this.vrButtonDiv.id = "vr-button";
            this.vrButtonDiv.innerHTML = "&nbsp";

            document.body.appendChild(this.vrButtonDiv);

            this.infoBoxDiv = document.createElement("div");
            this.infoBoxDiv.id = "info-box";

            this.infoBoxLifeTime = 0;
            this.infoBoxShowing = false;

            this.vrEntered = false;

            var self = this;

            var onEnterVrPressedEvent = function() {
                // If WebVR is available and a VrDisplay is attached
                if (self.app.vr && self.app.vr.display) {
                    if (self.vrEntered) {
                        // Exit vr (needed for non-mobile)
                        self.camera.camera.exitVr(function (err) {
                            if (err) {
                                // console.warn('ExitVr: '+err);
                                // self.entity.maze.message('ExitVr: '+err);
                            }
                        });
                    }  else {
                        // Enter vr
                        self.camera.camera.enterVr(function (err) {
                            if (err) {
                                // console.warn('EnterVr: '+err);
                                // self.entity.maze.message('EnterVr: '+err);
                            }
                        });
                    }
                } else {
                    if (!self.infoBoxShowing) {
                        if (self.app.vr.isSupported) {
                            self.infoBoxDiv.innerHTML = "No VR display or headset is detected.";
                        }  else {
                            self.infoBoxDiv.innerHTML = "Sorry, your browser does not support WebVR :(. Please go <a href='https://webvr.info/' target='_blank'>here</a> for more information.";
                        }

                        self.infoBoxLifeTime = self.infoBoxLifeSpan;
                        document.body.appendChild(self.infoBoxDiv);
                        self.infoBoxShowing = true;
                    }
                }
            };

            this.vrButtonDiv.addEventListener('click', onEnterVrPressedEvent, false);

            // try and enter vr immediately for Carmel browser
            onEnterVrPressedEvent();
        };

        PlayerVrUi.prototype.isMobile = function() {
            return /Android/i.test(navigator.userAgent) ||
                /iPhone|iPad|iPod/i.test(navigator.userAgent);
        };

        // update code called every frame
        PlayerVrUi.prototype.update = function(dt) {
            if (this.infoBoxShowing) {
                this.infoBoxLifeTime -= dt;
                if (this.infoBoxLifeTime <= 0) {
                    document.body.removeChild(this.infoBoxDiv);
                    this.infoBoxShowing = false;
                }
            }
        };

        PlayerVrUi.prototype.onVrPresentChange = function(display) {
            if (display.presenting) {
                // Only remove the VR button if we are on mobile
                if (this.isMobile()) {
                    document.body.removeChild(this.vrButtonDiv);
                }
                this.vrEntered = true;
            } else {
                if (this.isMobile()) {
                    document.body.appendChild(this.vrButtonDiv);
                }
                this.vrEntered = false;
            }
        };
    }
})('PlayerVrUi');

(function(name) {
    if (!pc.Application.getApplication().scripts.has(name)) {
        var playerCamera = pc.createScript(name);

        playerCamera.attributes.add("mouseLookSensitivity", {type: "number", default: 0, title: "Mouse Look Sensitivity", description: ""});
        playerCamera.attributes.add("touchLookSensitivity", {type: "number", default: 0, title: "Touch Look Sensitivity", description: ""});

        playerCamera.prototype.initialize = function () {
            // Camera euler angle rotation around x and y axes
            var quat = this.entity.getLocalRotation();
            this.ex = this.getPitch(quat) * pc.math.RAD_TO_DEG;
            this.ey = this.getYaw(quat) * pc.math.RAD_TO_DEG;

            this.targetEx = this.ex;
            this.targetEy = this.ey;

            this.moved = false;
            this.lmbDown = false;

            // Disabling the context menu stops the browser displaying a menu when
            // you right-click the page
            // this.app.mouse.disableContextMenu();


            this.lastTouchPosition = new pc.Vec2();

            this.addEventCallbacks();

            this.on("destroy", function () {
                this.removeEventCallbacks();
            });

            if (this.app.vr && this.app.vr.display) {
                this.app.vr.display.on("presentchange", this.onVrPresentChange, this);
            }

            this.startCameraOrientation = this.entity.getLocalRotation().clone();
        };

        playerCamera.prototype.addEventCallbacks = function() {
            if (this.app.touch) {
                // this.app.touch.on(pc.EVENT_TOUCHSTART, this.onTouchStart, this);
                // this.app.touch.on(pc.EVENT_TOUCHEND, this.onTouchEnd, this);
                this.app.touch.on(pc.EVENT_TOUCHMOVE, this.onTouchMove, this);
                this.app.touch.on(pc.EVENT_TOUCHCANCEL, this.onTouchCancel, this);
            }
            if (this.app.mouse) {
                this.app.mouse.on(pc.EVENT_MOUSEMOVE, this.onMouseMove, this);
                this.app.mouse.on(pc.EVENT_MOUSEDOWN, this.onMouseDown, this);
                this.app.mouse.on(pc.EVENT_MOUSEUP, this.onMouseUp, this);
            }
        };

        playerCamera.prototype.removeEventCallbacks = function() {
            if (this.app.touch) {
                // this.app.touch.off(pc.EVENT_TOUCHSTART, this.onTouchStart, this);
                // this.app.touch.off(pc.EVENT_TOUCHEND, this.onTouchEnd, this);
                this.app.touch.off(pc.EVENT_TOUCHMOVE, this.onTouchMove, this);
                this.app.touch.off(pc.EVENT_TOUCHCANCEL, this.onTouchCancel, this);
            }
            if (this.app.mouse) {
                this.app.mouse.off(pc.EVENT_MOUSEMOVE, this.onMouseMove, this);
                this.app.mouse.off(pc.EVENT_MOUSEDOWN, this.onMouseDown, this);
                this.app.mouse.off(pc.EVENT_MOUSEUP, this.onMouseUp, this);
            }
        };

        // Taken from http://stackoverflow.com/questions/23310299/quaternion-from-tait-bryan-angles
        // Not completely accurate, usually a couple of degrees out
        playerCamera.prototype.getYaw = function (quaternion) {
            var q = quaternion;
            var x = q.x * q.x;
            var y = q.y * q.y;

            return Math.atan2(2 * q.y * q.w - 2 * q.z * q.x, 1 - 2 * y - 2 * x);
        };

        playerCamera.prototype.getPitch = function(quaternion) {
            var q = quaternion;
            return -Math.asin(2 * q.z * q.y + 2 * q.x * q.w);
        };

        playerCamera.prototype.update = function (dt) {
            // Update the camera's orientation
            this.ex = pc.math.lerp(this.ex, this.targetEx, dt / 0.2);
            this.ey = pc.math.lerp(this.ey, this.targetEy, dt / 0.2);
            // if (!this.app.vr || !this.app.vr.display || !this.app.vr.display.presenting) {
                this.entity.setLocalEulerAngles(this.ex, this.ey, 0);
            // }
        };

        playerCamera.prototype.onVrPresentChange = function(display) {
            if (display.presenting) {
                this.removeEventCallbacks();
            } else {
                this.addEventCallbacks();
                // this.entity.setLocalEulerAngles(0,0,0);
            }
        };

        playerCamera.prototype.moveCamera = function(dx, dy, lookSensitivity) {
            // Update the current Euler angles, clamp the pitch.
            if (!this.moved) {
                // first move event can be very large
                this.moved = true;
                return;
            }

            this.targetEx += dy * lookSensitivity;
            this.targetEx = pc.math.clamp(this.targetEx, -90, 90);
            this.targetEy += dx * lookSensitivity;
        };

        playerCamera.prototype.onMouseMove = function (event) {
            // if (!this.lmbDown)
            //     return;
            // this.entity.maze.message('Mouse: '+(pc.Mouse.isPointerLocked()event.buttons[0] ? 'On' : 'Off')+', dx: '+event.dx+', dy: '+event.dy);
            // this.entity.maze.message('Mouse: '+(pc.Mouse.isPointerLocked() ? 'On' : 'Off')+', dx: '+event.dx+', dy: '+event.dy);
            if (pc.Mouse.isPointerLocked()) {
                if (!this.app.vr || !this.app.vr.display || !this.app.vr.display.presenting) {
                    this.moveCamera(-event.dx, -event.dy, this.mouseLookSensitivity);
                    return;
                }
            }
            event.event.preventDefault();
        };

        playerCamera.prototype.onMouseDown = function (event) {
            if (!pc.Mouse.isPointerLocked())
                this.app.mouse.enablePointerLock();
            // this.entity.maze.message('Mouse: Down');
            // if (event.button === 0) {
            //     this.lmbDown = true;
            // }
        };

        playerCamera.prototype.onMouseUp = function (event) {
            // if (event.button === 0) {
            //     this.lmbDown = false;
            // }
        };

        playerCamera.prototype.onTouchStart = function(event) {
            // this.entity.maze.message('Touch: Start');
            if (!pc.Mouse.isPointerLocked())
                this.app.mouse.enablePointerLock();
            if (event.touches.length == 1) {
                this.lmbDown = true;
                var touch = event.touches[0];
                this.lastTouchPosition.set(touch.x, touch.y);
            }
            event.event.preventDefault();
        };

        playerCamera.prototype.onTouchEnd = function(event) {
            // this.entity.maze.message('Touch: End');
            if (event.touches.length === 0) {
                this.lmbDown = false;
            } else if (event.touches.length == 1) {
                var touch = event.touches[0];
                this.lastTouchPosition.set(touch.x, touch.y);
            }
        };

        playerCamera.prototype.onTouchMove = function(event) {
            var touch = event.touches[0];
            // this.entity.maze.message('Touch: Move('+event.touches.length+') x: '+touch.x+' y: '+touch.y);
            if (event.touches.length == 1) {
                this.moveCamera((touch.x - this.lastTouchPosition.x), (touch.y - this.lastTouchPosition.y), this.touchLookSensitivity);
            }
            this.lastTouchPosition.set(touch.x, touch.y);
        };

        playerCamera.prototype.onTouchCancel = function(event) {
            // this.entity.maze.message('Touch: Cancel');
            this.lmbDown = false;
        };
    }
})('PlayerCamera');

(function(name) {
    if (!pc.Application.getApplication().scripts.has(name)) {
        var playerMove = pc.createScript(name);

        // optional, assign a camera entity, otherwise one is created
        playerMove.attributes.add('camera', {
            type: 'entity'
        });

        playerMove.attributes.add('power', {
            type: 'number'
        });

        playerMove.attributes.add('lookSpeed', {
            type: 'number'
        });

        // initialize code called once per entity
        playerMove.prototype.initialize = function () {
            this.force = new pc.Vec3();
            this.camera = null;
            this.eulers = new pc.Vec3();
            this.touchGo = false;
            this.keyboard = new pc.Keyboard(document.body);

            // Listen for mouse move events
            // if (this.app.touch) {
            //     this.app.mouse.on("mousemove", this._onMouseMove, this);
            // }
            if (this.app.touch) {
                this.app.touch.on(pc.EVENT_TOUCHSTART, this._onTouchStart, this);
                this.app.touch.on(pc.EVENT_TOUCHEND, this._onTouchEnd, this);
            }

            // // when the mouse is clicked hide the cursor
            // app.mouse.on("mousedown", function () {
            //     app.mouse.enablePointerLock();
            // }, this);

            // this.keyboard.on("keydown", function (e) {
            //     this.entity.maze.message('Key: '+e.key+': '+e.event.key);
            //     // console.log(e);
            //     e.event.preventDefault();
            // }, this);

            // Check for required components
            if (!this.entity.collision) {
                console.error("First Person Movement script needs to have a 'collision' component");
            }

            if (!this.entity.rigidbody || this.entity.rigidbody.type !== pc.BODYTYPE_DYNAMIC) {
                console.error("First Person Movement script needs to have a DYNAMIC 'rigidbody' component");
            }
            this._createCamera();
        };

        // update code called every frame
        playerMove.prototype.update = function (dt) {
            // If a camera isn't assigned from the Editor, create one
            if (!this.camera) {
                this._createCamera();
            }
            if (!this.app.vr || !this.app.vr.display || !this.app.vr.display.presenting) {
                if (this.touchGo)
                    this.touchGo = false;
            }
            // if (this.app.vr && this.app.vr.display && this.app.vr.display.presenting) {
            //     this.app.mouse.on("mousemove", this._onMouseMove, this);
            // }
            // if (!pc.Mouse.isPointerLocked())
            //     this.app.mouse.enablePointerLock();

            var force = this.force;

            // Get camera directions to determine movement directions
            var forward = this.camera.forward;
            var right = this.camera.right;


            // movement
            var x = 0;
            var z = 0;

            // Use W-A-S-D keys to move player
            // Check for key presses
            if (this.keyboard.isPressed(pc.KEY_LEFT) || this.keyboard.isPressed(pc.KEY_A)) {
                // this.camera.rotate(0, 5, 0);
                x -= right.x;
                z -= right.z;
            }

            if (this.keyboard.isPressed(pc.KEY_RIGHT) || this.keyboard.isPressed(pc.KEY_D)) {
                // this.camera.rotate(0, -5, 0);
                x += right.x;
                z += right.z;
            }

            if (this.touchGo || this.keyboard.isPressed(pc.KEY_UP) || this.keyboard.isPressed(pc.KEY_W)) {
                x += forward.x;
                z += forward.z;
            }

            if (this.keyboard.isPressed(pc.KEY_DOWN) || this.keyboard.isPressed(pc.KEY_S)) {
                x -= forward.x;
                z -= forward.z;
            }

            // use direction from keypresses to apply a force to the character
            if (x !== 0 || z !== 0) {
                force.set(x, 0, z).normalize().scale(this.power);
                this.entity.rigidbody.applyForce(force);
                // this.entity.maze.message('Key: '+force.toString());
                this.entity.maze.showMap();
            }

            // update camera angle from mouse events
            // this.camera.setLocalEulerAngles(this.eulers.y, this.eulers.x, 0);
        };

        playerMove.prototype.getYangle = function () {
            var q = this.camera.getLocalRotation();
            var x = q.x * q.x;
            var y = q.y * q.y;
            var ang = Math.atan2(2 * q.y * q.w - 2 * q.z * q.x, 1 - 2 * y - 2 * x);
            return ang * pc.math.RAD_TO_DEG;
        };


        playerMove.prototype._onTouchStart = function(event) {
            // this.entity.maze.message('Touch: Start');
            // If it's not VR display -- touch - rotate camera
            if (!this.app.vr || !this.app.vr.display || !this.app.vr.display.presenting) {
                if (!pc.Mouse.isPointerLocked())
                    this.app.mouse.enablePointerLock();
                if (event.touches.length == 1) {
                    var camScript = this.camera.script['PlayerCamera'];
                    camScript.lmbDown = true;
                    var touch = event.touches[0];
                    camScript.lastTouchPosition.set(touch.x, touch.y);
                }
                return;
            }
            this.touchGo = true;
            return true;
            // event.event.preventDefault();
        };

        playerMove.prototype._onTouchEnd = function(event) {
            // this.entity.maze.message('Touch: End');
            // If it's not VR display -- touch - rotate camera
            if (!this.app.vr || !this.app.vr.display || !this.app.vr.display.presenting) {
                var camScript = this.camera.script['PlayerCamera'];
                if (event.touches.length === 0) {
                    camScript.lmbDown = false;
                } else if (event.touches.length == 1) {
                    var touch = event.touches[0];
                    camScript.lastTouchPosition.set(touch.x, touch.y);
                }
                return;
            }
            this.touchGo = false;
            return true;
            // event.event.preventDefault();
        };

        playerMove.prototype._onMouseMove = function (e) {
            // If it's not VR display -- mouse - rotate camera
            if (!this.app.vr || !this.app.vr.display || !this.app.vr.display.presenting) {
                if (pc.Mouse.isPointerLocked()) {
                    var camScript = this.camera.script['PlayerCamera'];
                    camScript.moveCamera(-e.dx, -e.dy, camScript.mouseLookSensitivity);
                }
                return;
            }

            // If VR display does presenting -- mouse - move player
            var force = this.force;

            // Get camera directions to determine movement directions
            var angle = this.getYangle();

            // movement
            var x = 0;
            var z = 0;
            if (angle >= 0) {
                if (angle <= 90) {
                    x -= e.dx * (1 - (angle / 90.0));
                    z += e.dx * (angle / 90.0);
                } else {
                    x += e.dx * ((angle - 90) / 90.0);
                    z += e.dx * (1 - ((angle - 90) / 90.0));
                }
            } else {
                if (angle >= -90) {
                    x -= e.dx * (1 + (angle / 90.0));
                    z += e.dx * ((angle / 90.0));
                } else {
                    x += e.dx * ( ((angle + 90) / 90.0));
                    z += e.dx * (1 + ((angle + 90) / 90.0));
                }
            }

            if (angle >= 0) {
                if (angle <= 90) {
                    x -= e.dy * (angle / 90.0);
                    z -= e.dy * (1 - (angle / 90.0));
                } else {
                    x -= e.dy * (1 - ((angle - 90) / 90.0));
                    z += e.dy * ((angle - 90) / 90.0);
                }
            } else {
                if (angle >= -90) {
                    x -= e.dy * (angle / 90.0);
                    z -= e.dy * (1 + (angle / 90.0));
                } else {
                    x += e.dy * (1 + ((angle + 90) / 90.0));
                    z -= e.dy * ((angle + 90) / 90.0);
                }
            }

            // use direction from keypresses to apply a force to the character
            if (x !== 0 || z !== 0) {
                force.set(x, 0, z).normalize().scale(this.power/4);
                this.entity.rigidbody.applyForce(force);
                // this.entity.maze.message('Mouse: '+angle.toFixed(3)+','+x.toFixed(3)+','+z.toFixed(3));
            }

            // this.entity.maze.message('Mouse: '+(pc.Mouse.isPointerLocked() ? 'On' : 'Off')+', dx: '+e.dx+', dy: '+e.dy);
            // if (pc.Mouse.isPointerLocked()) {
            //     // console.log(e);
            //     this.eulers.x -= this.lookSpeed * e.dx;
            //     this.eulers.y -= this.lookSpeed * e.dy;
            //     return;
            // }
            // e.event.preventDefault();
        };

        playerMove.prototype._createCamera = function () {
            // If user hasn't assigned a camera, create a new one
            this.camera = new pc.Entity();
            this.camera.setName("First Person Camera");
            this.camera.addComponent("camera");
            this.camera.addComponent("audiolistener");
            this.entity.addChild(this.camera);
            this.camera.translateLocal(0, 1, 0);
        };
    }
})('PlayerMove');

(function(name) {
    if (!pc.Application.getApplication().scripts.has(name)) {
        var player = pc.createScript(name);

        player.attributes.add('camera', {
            type: 'entity'
        });

        // initialize code called once per entity
        player.prototype.initialize = function () {
            var self = this;

            var skin = new pc.StandardMaterial();
            skin.diffuse = new pc.Color(1, 0.8, 0.2);
            skin.update();

            self.entity.addComponent("model", {
                type: "capsule",
                castShadows: true,
                castShadowsLightmap: true,
                receiveShadows: true
            });
            self.entity.model.material = skin;

            self.entity.addComponent("rigidbody", {
                type: "dynamic",
                mass: 50,
                linearDamping: 0.99,
                angularDamping: 0,
                linearFactor: new pc.Vec3(1, 1, 1),
                angularFactor: new pc.Vec3(0, 0, 0),
                friction: 0.75,
                restitution: 0.5
            });

            self.entity.addComponent("collision", {
                type: "capsule",
                radius: 0.5,
                height: 2
            });

            self.entity.script.create(name+'Move', {
                attributes: {
                    power: 2500,
                    lookSpeed: 0.1
                }
            });


            self.camera = self.entity.script[name+'Move'].camera;
            self.camera.maze = self.entity.maze;
            self.camera.addComponent('script');
            self.camera.script.create(name+'Camera', {
                attributes: {
                    mouseLookSensitivity: 0.1,
                    touchLookSensitivity: 0.2
                }
            });

            self.app.root.addComponent('script');
            self.app.root.maze = self.entity.maze;
            self.app.root.script.create(name+'VrUi', {
                attributes: {
                    camera: self.camera,
                    enterVrWhite: 'assets/cardboard-white.png',
                    enterVrOrange: 'assets/cardboard-orange.png',
                    infoBoxLifeSpan: 5
                }
            });

            self.entity.setLocalScale(1, 1, 1);
            self.entity.rigidbody.teleport(0, -1, 0);
            self.entity.rigidbody.activate();
        };
    }
})('Player');
