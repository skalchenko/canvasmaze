pc.script.create('player_camera', function (app) {
    var PlayerCamera = function (entity) {
        this.entity = entity;

        // Camera euler angle rotation around x and y axes
        this.ex = 0;
        this.ey = 0;

        // Disabling the context menu stops the browser displaying a menu when
        // you right-click the page
        app.mouse.disableContextMenu();
        app.mouse.on(pc.input.EVENT_MOUSEMOVE, this.onMouseMove, this);
        app.mouse.on(pc.input.EVENT_MOUSEDOWN, this.onMouseDown, this);
    };

    PlayerCamera.prototype = {
        update: function (dt) {
            // Update the camera's orientation
            this.entity.setEulerAngles(this.ex, this.ey, 0);
        },

        onMouseMove: function (event) {
            // Update the current Euler angles, clamp the pitch.
            this.ex -= event.dy / 5;
            this.ex = pc.math.clamp(this.ex, -90, 90);
            this.ey -= event.dx / 5;
        },

        onMouseDown: function (event) {
            // When the mouse button is clicked try and capture the pointer
            if (!pc.input.Mouse.isPointerLocked()) {
                app.mouse.enablePointerLock();
            }
        }
    };

    return PlayerCamera;
});
