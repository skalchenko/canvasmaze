var MultitouchInput = pc.createScript('multitouchInput');

MultitouchInput.attributes.add("cameraEntity", {type: "entity", title: "Camera Entity"});
MultitouchInput.attributes.add("lineColor", {type: "rgba", title: "Line Color"});

MultitouchInput.DEFAULT_DISTANCE_FROM_CAMERA = 4;

// initialize code called once per entity
MultitouchInput.prototype.initialize = function () {
    // Only register these touch event callbacks if touch is available
    var touch = this.app.touch;
    if (touch) {
        touch.on(pc.EVENT_TOUCHSTART, this.onTouchStart, this);
        touch.on(pc.EVENT_TOUCHMOVE, this.onTouchMove, this);
        touch.on(pc.EVENT_TOUCHEND, this.onTouchEnd, this);
        touch.on(pc.EVENT_TOUCHCANCEL, this.onTouchCancel, this);
    } else {
        // Touch isn't supported on device (should show a message in the sample)
    }

    // Keep track of the fingers on the screen
    this.activeTouches = [];

    // Keep a cache of 'touches' so we don't keep creating new ones every time the
    // user touches the screen
    this.freeTouches = [];
};

// update code called every frame
MultitouchInput.prototype.update = function(dt) {
    // Draw a line from the first touch object to the next and then from that one to the next, etc
    for (var i = 1; i < this.activeTouches.length; i += 1) {
        var prevTouch = this.activeTouches[i-1];
        var currentTouch = this.activeTouches[i];

        this.app.renderLine(prevTouch.position, currentTouch.position, this.lineColor);
    }
};


MultitouchInput.prototype.addTouchWorldPosition = function (touch) {
    var freeTouch = null;

    // Use a free touch object from the cache or create a new one if the cache is empty
    if (this.freeTouches.length > 0) {
        freeTouch = this.freeTouches.pop();
    } else {
        freeTouch = { id : 0, position: new pc.Vec3() };
    }

    // The way that touches are tracked is that every touch is assigned an ID which doesn't get unassgined
    // until that touch has left the screen. So when a user touches the screen with their finger, that 'finger'
    // will be assigned an ID which doesn't not change until that finger leaves the screen
    freeTouch.id = touch.id;

    // Project the screen touch position to a world position
    // (The distance from camera doesn't matter in this example as long as it's a constant value)
    this.cameraEntity.camera.screenToWorld(touch.x, touch.y, MultitouchInput.DEFAULT_DISTANCE_FROM_CAMERA, freeTouch.position);

    // Add the touch object to active list so we can draw lines from one touch object to the next
    this.activeTouches.push(freeTouch);
};


MultitouchInput.prototype.updateTouchWorldPosition = function (touch) {
    // Find the touch object by the ID in the active list and update the world position of it
    for (var i = 0; i < this.activeTouches.length; i += 1) {
        var activeTouch = this.activeTouches[i];
        if (touch.id == activeTouch.id) {
            this.cameraEntity.camera.screenToWorld(touch.x, touch.y, MultitouchInput.DEFAULT_DISTANCE_FROM_CAMERA, activeTouch.position);
            return;
        }
    }
};


MultitouchInput.prototype.removeTouchWorldPosition = function (touch) {
    var indexToRemove = -1;
    var i = 0;

    // Found the touch object of the finger that has been removed from the screen
    for (i = 0; i < this.activeTouches.length; i += 1) {
        var activeTouch = this.activeTouches[i];
        if (touch.id == activeTouch.id) {
            indexToRemove = i;
            break;
        }
    }

    // Remove it from the active list and add to the free touch object cache
    if (indexToRemove != -1) {
        var removedTouchObjects = this.activeTouches.splice(indexToRemove, 1);
        for (i = 0; i < removedTouchObjects.length; i += 1) {
            this.freeTouches.push(removedTouchObjects[i]);
        }
    }
};


MultitouchInput.prototype.onTouchStart = function (touchEvent) {
    // The changed touches for the 'touch start' event should only contain new touches
    // (i.e. fingers that have just touched the screen)
    var changedTouches = touchEvent.changedTouches;
    for (var i = 0; i < changedTouches.length; i += 1) {
        this.addTouchWorldPosition(changedTouches[i]);
    }
};


MultitouchInput.prototype.onTouchMove = function (touchEvent) {
    // The changed touches for 'touch move' event should only contain touches that have
    // moved (i.e. fingers that moving across screen)
    var changedTouches = touchEvent.changedTouches;
    for (var i = 0; i < changedTouches.length; i += 1) {
        this.updateTouchWorldPosition(changedTouches[i]);
    }
};


MultitouchInput.prototype.onTouchEnd = function (touchEvent) {
    // The changed touches for 'touch end' event should only contain touches that have
    // removed (i.e. fingers that are no longer touching the screen)
    var changedTouches = touchEvent.changedTouches;
    for (var i = 0; i < changedTouches.length; i += 1) {
        this.removeTouchWorldPosition(changedTouches[i]);
    }
};


MultitouchInput.prototype.onTouchCancel = function (touchEvent) {
    // The changed touches for 'touch end' event should only contain touches that have
    // gone out of bounds or otherwise have to removed for any other reason
    var changedTouches = touchEvent.changedTouches;
    for (var i = 0; i < changedTouches.length; i += 1) {
        this.removeTouchWorldPosition(changedTouches[i]);
    }
};
