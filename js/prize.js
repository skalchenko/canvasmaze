(function(name) {
    if (!pc.Application.getApplication().scripts.has(name)) {
        var prize = pc.createScript(name);
        prize.attributes.add('speed', {
            type: 'number',
            default: 0.5
        });
        // initialize code called once per entity
        prize.prototype.initialize = function () {
            this.inside = {
                doMoving: false,
                step: 2,
                cnt: 0
            };
        };

        prize.prototype.update = function(dt) {
            var playerPos = this.entity.maze.player.getPosition();
            var prizePos = this.entity.maze.prize.getPosition();
            var dx = Math.abs(playerPos.x - prizePos.x);
            var dy = Math.abs(playerPos.z - prizePos.z);
            var dist = Math.sqrt(dx * dx + dy * dy);
            // this.entity.maze.info(playerPos.x.toFixed(3) + ':' + playerPos.z.toFixed(3) + ' -> ' + prizePos.x.toFixed(3) + ':' + prizePos.z.toFixed(3) + '=' + dist.toFixed(3));

            if (!this.inside.doMoving && (dist < 2.0)) {
                this.inside.doMoving = true;
                this.entity.maze.movePrize();
            } else if (this.inside.doMoving && (dist > 2.0)) {
                this.inside.doMoving = false;
            }

            this.entity.rotate(0, this.speed, 0);
        };
    }
})('PrizeAction');

(function(name) {
    if (!pc.Application.getApplication().scripts.has(name)) {
        var prize = pc.createScript(name);

        prize.attributes.add('winX', {
            type: 'number'
        });
        prize.attributes.add('winY', {
            type: 'number'
        });
        prize.attributes.add('winZ', {
            type: 'number'
        });

        prize.attributes.add('soundDist', {
            type: 'number'
        });

        // initialize code called once per entity
        prize.prototype.initialize = function () {
            var self = this;
            self.inside = {
                // model: 'assets/statue/statue1.json',
                model: 'assets/chest/Chest.json',
                sound: 'assets/bensound-tenderness.mp3',
                // dx: 0,
                // dy: -2,
                // dz: 0,
                // sx: 0.2,
                // sy: 0.2,
                // sz: 0.2,
                dx: 0,
                dy: -1.1,
                dz: 0,
                sx: 2,
                sy: 2,
                sz: 2,
                hasModel: false,
                hasSound: false,
                aModel: null,
                aSound: null
            };

            // var onPrizeMove = function(x, y, z) {
            //     self.winX = x;
            //     self.winY = y;
            //     self.winZ = z;
            //     var prizePos = self.entity.getPosition();
            //     // self.entity.maze.log(prizePos.x.toFixed(3) + ':' + prizePos.z.toFixed(3) + ' -> ' + self.winX.toFixed(3) + ':' + self.winZ.toFixed(3));
            //     self.entity.maze.prize.setPosition(self.winX + self.inside.dx, self.winY + self.inside.dy, self.winZ + self.inside.dz);
            // };
            // self.app.on('prize:move', onPrizeMove);

            self.entity.moveTo = function(x, y, z) {
                self.winX = x;
                self.winY = y;
                self.winZ = z;
                var prizePos = self.entity.getPosition();
                // self.entity.maze.log(prizePos.x.toFixed(3) + ':' + prizePos.z.toFixed(3) + ' -> ' + self.winX.toFixed(3) + ':' + self.winZ.toFixed(3));
                self.entity.setPosition(self.winX + self.inside.dx, self.winY + self.inside.dy, self.winZ + self.inside.dz);
            };

            var initPrize = function() {
                if (self.inside.hasModel && self.inside.hasSound) {
                    // self.app.fire('prize:move', self.winX, self.winY, self.winZ);

                    self.entity.addComponent("model");
                    self.entity.model.model = self.inside.aModel;
                    self.entity.setLocalScale(self.inside.sx, self.inside.sy, self.inside.sz);
                    // self.entity.setPosition(self.winX + self.inside.dx, self.winY + self.inside.dy, self.winZ + self.inside.dz);
                    self.entity.moveTo(self.winX, self.winY, self.winZ);

                    self.entity.addComponent("rigidbody", {
                        type: "kinematic",
                        friction: 0.1,
                        restitution: 0.1
                    });
                    self.entity.addComponent("collision", {
                        type: "cylinder",
                        radius: 0.5,
                        height: 4
                    });
                    self.entity.script.create('PrizeAction', {
                        attributes: {
                            speed: 0.5
                        }
                    });

                    if (self.inside.aSound) {
                        self.entity.addComponent('sound');
                        self.entity.sound.positional = true;
                        self.entity.sound.volume = 0.5;
                        self.entity.sound.distanceModel = pc.DISTANCE_EXPONENTIAL;
                        self.entity.sound.refDistance = 2;
                        self.entity.sound.maxDistance = self.soundDist;

                        self.entity.sound.addSlot('music', {
                            asset: self.inside.aSound,
                            // pitch: 1.7,
                            loop: true,
                            autoPlay: true
                        });
                    }

                }
            };
            self.app.assets.loadFromUrl(self.inside.model, "model", function (err, asset) {
                self.inside.aModel = asset.resource;
                self.inside.hasModel = true;
                initPrize();
            });
            if (self.inside.sound && self.inside.sound !== '') {
                self.app.assets.loadFromUrl(self.inside.sound, "audio", function (err, asset) {
                    self.inside.aSound = asset;
                    self.inside.hasSound = true;
                    initPrize();
                });
            } else {
                self.inside.hasSound = true;
                initPrize();
            }
        };
    }
})('Prize');
