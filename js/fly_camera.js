var LookCamera = pc.createScript('lookCamera');

LookCamera.attributes.add("mouseLookSensitivity", {type: "number", default: 0, title: "Mouse Look Sensitivity", description: ""});
LookCamera.attributes.add("touchLookSensitivity", {type: "number", default: 0, title: "Touch Look Sensitivity", description: ""});

LookCamera.prototype.initialize = function () {
    // Camera euler angle rotation around x and y axes
    var quat = this.entity.getLocalRotation();
    this.ex = this.getPitch(quat) * pc.math.RAD_TO_DEG;
    this.ey = this.getYaw(quat) * pc.math.RAD_TO_DEG;
    
    this.targetEx = this.ex;
    this.targetEy = this.ey;
            
    this.moved = false;
    this.lmbDown = false;

    // Disabling the context menu stops the browser displaying a menu when
    // you right-click the page
    // this.app.mouse.disableContextMenu();

    this.addEventCallbacks();
    
    this.lastTouchPosition = new pc.Vec2();
    
    this.on("destroy", function () {
        this.removeEventCallbacks();
    });
    
    if (this.app.vr && this.app.vr.display) {
        this.app.vr.display.on("presentchange", this.onVrPresentChange, this);
    }
    
    this.startCameraOrientation = this.entity.getLocalRotation().clone();
};

LookCamera.prototype.addEventCallbacks = function() {
    if (this.app.mouse) {
        this.app.mouse.on(pc.EVENT_MOUSEMOVE, this.onMouseMove, this);
        this.app.mouse.on(pc.EVENT_MOUSEDOWN, this.onMouseDown, this);
        this.app.mouse.on(pc.EVENT_MOUSEUP, this.onMouseUp, this);
    }
    
    if (this.app.touch) {
        this.app.touch.on(pc.EVENT_TOUCHSTART, this.onTouchStart, this);
        this.app.touch.on(pc.EVENT_TOUCHEND, this.onTouchEnd, this);
        this.app.touch.on(pc.EVENT_TOUCHMOVE, this.onTouchMove, this);
        this.app.touch.on(pc.EVENT_TOUCHCANCEL, this.onTouchCancel, this);
    }
};

LookCamera.prototype.removeEventCallbacks = function() {
    if (this.app.mouse) {
        this.app.mouse.off(pc.EVENT_MOUSEMOVE, this.onMouseMove, this);
        this.app.mouse.off(pc.EVENT_MOUSEDOWN, this.onMouseDown, this);
        this.app.mouse.off(pc.EVENT_MOUSEUP, this.onMouseUp, this);
    }

    if (this.app.touch) {
        this.app.touch.off(pc.EVENT_TOUCHSTART, this.onTouchStart, this);
        this.app.touch.off(pc.EVENT_TOUCHEND, this.onTouchEnd, this);
        this.app.touch.off(pc.EVENT_TOUCHMOVE, this.onTouchMove, this);
        this.app.touch.off(pc.EVENT_TOUCHCANCEL, this.onTouchCancel, this);
    }
};

// Taken from http://stackoverflow.com/questions/23310299/quaternion-from-tait-bryan-angles
// Not completely accurate, usually a couple of degrees out
LookCamera.prototype.getYaw = function (quaternion) {
    var q = quaternion;
    var x = q.x * q.x;
    var y = q.y * q.y;
    
    return Math.atan2(2 * q.y * q.w - 2 * q.z * q.x, 1 - 2 * y - 2 * x);    
};

LookCamera.prototype.getPitch = function(quaternion) {
    var q = quaternion;
    return -Math.asin(2 * q.z * q.y + 2 * q.x * q.w);
};

LookCamera.prototype.update = function (dt) {
    // Update the camera's orientation
    this.ex = pc.math.lerp(this.ex, this.targetEx, dt / 0.2);
    this.ey = pc.math.lerp(this.ey, this.targetEy, dt / 0.2);
    this.entity.setLocalEulerAngles(this.ex, this.ey, 0);
};

LookCamera.prototype.onVrPresentChange = function(display) {
    if (display.presenting) {
        this.removeEventCallbacks();
    }
    else {
        this.addEventCallbacks();
        this.entity.setLocalEulerAngles(0,0,0);
    }
};

LookCamera.prototype.moveCamera = function(dx, dy, lookSensitivity) {
    // Update the current Euler angles, clamp the pitch.
    if (!this.moved) {
        // first move event can be very large
        this.moved = true;
        return;
    }
        
    this.targetEx += dy * lookSensitivity;
    this.targetEx = pc.math.clamp(this.targetEx, -90, 90);
    this.targetEy += dx * lookSensitivity;   
};

LookCamera.prototype.onMouseMove = function (event) {    
    // if (!this.lmbDown)
    //     return;
    if (pc.Mouse.isPointerLocked()) {
        if (!this.app.vr || !this.app.vr.display || !this.app.vr.display.presenting) {
            this.moveCamera(-event.dx, -event.dy, this.mouseLookSensitivity);
            return;
        }
    }
    event.event.preventDefault();
};

LookCamera.prototype.onMouseDown = function (event) {
    if (!pc.Mouse.isPointerLocked())
        this.app.mouse.enablePointerLock();
    // if (event.button === 0) {
    //     this.lmbDown = true;
    // }
};

LookCamera.prototype.onMouseUp = function (event) {
    // if (event.button === 0) {
    //     this.lmbDown = false;
    // }
};

LookCamera.prototype.onTouchStart = function(event) {
    if (event.touches.length == 1) {
        this.lmbDown = true;
        var touch = event.touches[0];
        this.lastTouchPosition.set(touch.x, touch.y);
    }
    event.event.preventDefault();
};

LookCamera.prototype.onTouchEnd = function(event) {
    if (event.touches.length === 0) {
        this.lmbDown = false;
    } else if (event.touches.length == 1) {
        var touch = event.touches[0];
        this.lastTouchPosition.set(touch.x, touch.y);  
    }
};

LookCamera.prototype.onTouchMove = function(event) {
    var touch = event.touches[0];
    if (event.touches.length == 1) {
        this.moveCamera((touch.x - this.lastTouchPosition.x), (touch.y - this.lastTouchPosition.y), this.touchLookSensitivity);
    }
    this.lastTouchPosition.set(touch.x, touch.y);
};

LookCamera.prototype.onTouchCancel = function(event) {
    this.lmbDown = false;
};