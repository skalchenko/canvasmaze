function Maze(xx, yy, canvasId, messageId) {
    var self = this;

    var inside = {
        isInit: false,
        isGenerated:false,
        isPushing:  false,
        isPushUp:   false,
        loadPlayer: false,
        hasPlayer:  false,
        loadPrise:  false,
        hasPrise:   false,
        sizeX:  0,
        sizeY:  0,
        linesH: [],
        linesV: [],
        pc:     null,
        itemWindow: null,
        itemMap:    null,
        itemInfo:   null,
        itemLog:    null,
        app:    null,
        materialFloor:  null,
        materialWall:   null,
        materialPictures:   [],
        loadMaterials:  0,
        ground:     null,
        level:      null,
        pushTo:     null,
        pushStep:   null,
        pushPeriod: 200,
        winX:   0,
        winY:   0,
        winZ:   0
    };

    //-- Private methods
    inside.init = function() {
        if (!inside.isInit) {
            inside.pc = pc;
            inside.itemWindow = document.getElementById(canvasId);

            inside.app = new inside.pc.Application(inside.itemWindow, {
                mouse: new inside.pc.Mouse(inside.itemWindow),
                touch: new inside.pc.TouchDevice(inside.itemWindow),
                vr: true
            });
            inside.app.setCanvasFillMode(inside.pc.FILLMODE_FILL_WINDOW);
            inside.app.setCanvasResolution(inside.pc.RESOLUTION_AUTO);

            // use device pixel ratio
            inside.app.graphicsDevice.maxPixelRatio = window.devicePixelRatio;

            inside.app.start();

            // Set the gravity for our rigid bodies
            inside.app.systems.rigidbody.setGravity(0, -9.8, 0);

            // hook up window resize events
            window.addEventListener("resize", function () {
                inside.app.resizeCanvas(inside.itemWindow.width, inside.itemWindow.height);
            });

            inside.ground = new inside.pc.Entity();
            inside.app.root.addChild(inside.ground);

            inside.level  = new inside.pc.Entity();
            inside.app.root.addChild(inside.level);

            inside.pushTo   = new pc.Vec3(0, -3.5, 0);
            inside.pushStep = new pc.Vec3(0, 0.1, 0);

            self.setWallColor('#BBBBBB');
            self.setFloorColor('#444444');

            inside.sizeY = yy;
            inside.sizeX = xx;

            inside.isInit = true;
        }
    };

    inside.generate = function() {
        var n = inside.sizeX * inside.sizeY - 1;
        if (n < 0) {
            self.log("Illegal maze dimensions");
            return;
        }
        inside.linesH = []; for (var j= 0; j < inside.sizeY+1; j++) inside.linesH[j] = [];
        inside.linesV = []; for (var j= 0; j < inside.sizeY+1; j++) inside.linesV[j] = [];
        var here = [Math.floor(Math.random()*inside.sizeY), Math.floor(Math.random()*inside.sizeX)];
        var path = [here];
        var unvisited = [];
        for (var j = 0; j < inside.sizeY+2; j++) {
            unvisited[j] = [];
            for (var k = 0; k < inside.sizeX+1; k++)
                unvisited[j].push(j > 0 && j < inside.sizeY+1 && k > 0 && (j !== here[0]+1 || k !== here[1]+1));
        }
        while (0 < n) {
            var potential = [[here[0]+1, here[1]], [here[0],here[1]+1],
                [here[0]-1, here[1]], [here[0],here[1]-1]];
            var neighbors = [];
            for (var j = 0; j < 4; j++)
                if (unvisited[potential[j][0]+1][potential[j][1]+1])
                    neighbors.push(potential[j]);
            if (neighbors.length) {
                n = n-1;
                next= neighbors[Math.floor(Math.random()*neighbors.length)];
                unvisited[next[0]+1][next[1]+1]= false;
                if (next[0] === here[0])
                    inside.linesH[next[0]][(next[1]+here[1]-1)/2] = true;
                else
                    inside.linesV[(next[0]+here[0]-1)/2][next[1]] = true;
                path.push(here = next);
            } else
                here = path.pop();
        }
        inside.winX = (inside.sizeX - 1) * 4;
        inside.winY = 0;
        inside.winZ = (inside.sizeY - 1) * 4;
        inside.isGenerated = true;
    };

    inside.createSheet = function(parent,x,y,z,direct,material) {
        var sheet = new inside.pc.Entity();
        sheet.addComponent("model", {
            type: "box",
            castShadows: true
        });
        sheet.addComponent("rigidbody", {
            type: "kinematic",
            friction: 0.5,
            restitution: 0.5
        });
        sheet.addComponent("collision", {
            type: "box",
            halfExtents: new inside.pc.Vec3((direct === 0 ? 0.05 : 2), (direct === 1 ? 0.05 : 2), (direct === 2 ? 0.05 : 2))
        });
        sheet.translate(x, y, z);
        sheet.setLocalScale((direct===0 ? 0.1 : 4), (direct === 1 ? 0.1 : 4), (direct === 2 ? 0.1 : 4));
        sheet.model.material = material;
        // sheet.model.materialAsset = material;
        // sheet.model.model.meshInstances[0].material = material;
        parent.addChild(sheet);
        return sheet;
    };
    inside.createPicture = function(parent,x,y,z,direct) {
        var picture = inside.getPicture();
        if (picture != null) {
            var ratio = picture.sizeX / picture.sizeY;
            var sizeX, sizeY;
            if (ratio < 1.0) {
                sizeX = 2.0 * ratio;
                sizeY = 2.0;
            } else {
                sizeX = 2.0;
                sizeY = 2.0 / ratio;
            }

            var frame = new inside.pc.Entity();
            frame.addComponent("model", {
                type: "box",
                castShadows: true
            });
            frame.addComponent("rigidbody", {
                type: "kinematic",
                friction: 0.5,
                restitution: 0.5
            });
            frame.addComponent("collision", {
                type: "box",
                halfExtents: new inside.pc.Vec3((direct === 0 ? 0.055 : sizeX / 2 + 0.05), (direct === 1 ? 0.055 : sizeY / 2 + 0.05), (direct === 2 ? 0.055 : sizeX / 2 + 0.05))
            });
            frame.translate(x, y, z);
            frame.setLocalScale((direct===0 ? 0.11 : sizeX + 0.1), (direct === 1 ? 0.11 : sizeY + 0.1), (direct === 2 ? 0.11 : sizeX + 0.1));

            var frameColor = new inside.pc.Color();
            frameColor.fromString('#aaaaaa');
            var frameMaterial = new inside.pc.PhongMaterial();
            frameMaterial.diffuse = frameColor;
            frameMaterial.update();
            frame.model.material = frameMaterial;
            parent.addChild(frame);

            var sheet = new inside.pc.Entity();
            sheet.addComponent("model", {
                type: "box",
                castShadows: true
            });
            sheet.addComponent("rigidbody", {
                type: "kinematic",
                friction: 0.5,
                restitution: 0.5
            });
            sheet.addComponent("collision", {
                type: "box",
                halfExtents: new inside.pc.Vec3((direct === 0 ? 0.055 : sizeX / 2), (direct === 1 ? 0.055 : sizeY / 2), (direct === 2 ? 0.055 : sizeX / 2))
            });
            sheet.translate(x, y, z);
            sheet.setLocalScale((direct===0 ? 0.12 : sizeX), (direct === 1 ? 0.12 : sizeY), (direct === 2 ? 0.12 : sizeX));
            sheet.model.material = picture.material;
            parent.addChild(sheet);
            return sheet;
        }
        return null;
    };
    inside.getPicture = function() {
        if (inside.materialPictures.length > 0) {
            var i = Math.round(inside.pc.math.random(0, inside.materialPictures.length - 1));
            return inside.materialPictures[i];
        }
        return null;
    };
    inside.getWallMaterial = function() {
        // if (inside.wallMaterials.length > 0) {
        //     var i = Math.floor(inside.pc.math.random(0, inside.wallMaterials.length - 1));
        //     return inside.wallMaterials[i];
        // } else {
        //     return null;
        // }
        return inside.materialWall;
    };
    inside.getFloorMaterial = function() {
        return inside.materialFloor;
    };

    inside.display3d = function() {
        inside.isDisplaying = true;

        if (inside.loadMaterials) {
            setTimeout(inside.display3d, 500);
            return true;
        } else {
            for (var j = 0; j < inside.sizeY; j++) {
                // ---
                inside.createSheet(inside.ground, -2, 0, j * 4, 0, inside.getWallMaterial());
                inside.createSheet(inside.ground, inside.sizeX * 4 - 2, 0, j * 4, 0, inside.getWallMaterial());
                for (var k = 0; k < inside.sizeX; k++) {
                    if (j === 0) {
                        //  |
                        inside.createSheet(inside.ground, k * 4, 0, -2, 2, inside.getWallMaterial());
                        inside.createSheet(inside.ground, k * 4, 0, inside.sizeY * 4 - 2, 2, inside.getWallMaterial());
                    }
                    // floor
                    inside.createSheet(inside.ground, k * 4, -2, j * 4, 1, inside.getFloorMaterial());
                }
            }
            inside.isDisplayed = true;

            inside.lightOn();
        }
        return true;
    };

    inside.makeWalls = function() {
        if  (!inside.isDisplaying) {
            inside.display3d();
        }
        if (!inside.isDisplayed) {
            setTimeout(inside.makeWalls, 500);
            return;
        }
        if (!inside.isGenerated) {
            inside.generate();
        }

        var pos = inside.level.getPosition();
        inside.level.destroy();
        inside.level  = new inside.pc.Entity();
        inside.app.root.addChild(inside.level);
        inside.level.setPosition(pos);

        for (var j = 0; j < inside.sizeY; j++) {
            for (var k = 0; k < inside.sizeX; k++) {
                // ---
                if (k < inside.sizeX-1) {
                    if (!inside.linesH[j][k]) {
                        inside.createSheet(inside.level, k * 4 + 2, 0, j * 4, 0, inside.getWallMaterial());
                        inside.createPicture(inside.level, k * 4 + 2, 0, j * 4, 0);
                    }
                }
                //  |
                if (j < inside.sizeY-1) {
                    if (!inside.linesV[j][k]) {
                        inside.createSheet(inside.level, k * 4, 0, j * 4 + 2, 2, inside.getWallMaterial());
                        inside.createPicture(inside.level, k * 4, 0, j * 4 + 2, 2);
                    }
                }
            }
        }
    };
    inside.growDown = function() {
        if ((inside.isPushing === true) && (!inside.isPushUp)) {
            var pos = inside.level.getPosition();
            var toPos = new pc.Vec3(inside.pushTo.x, inside.pushTo.y, inside.pushTo.z);

            if (((toPos.x < 0) && (toPos.x < pos.x) || (toPos.x > 0) && (toPos.x > pos.x)) ||
                ((toPos.y < 0) && (toPos.y < pos.y) || (toPos.y > 0) && (toPos.y > pos.y)) ||
                ((toPos.z < 0) && (toPos.z < pos.z) || (toPos.z > 0) && (toPos.z > pos.z))) {

                var toStep = new pc.Vec3(inside.pushStep.x, inside.pushStep.y, inside.pushStep.z);
                if (toPos.x < 0)
                    toStep.x *= -1;
                if (toPos.y < 0)
                    toStep.y *= -1;
                if (toPos.z < 0)
                    toStep.z *= -1;

                inside.level.setPosition(pos.x + toStep.x, pos.y + toStep.y, pos.z + toStep.z);

                setTimeout(inside.growDown, inside.pushPeriod);
            } else {
                inside.generate();
                inside.makeWalls();
                self.showMap();

                inside.isPushUp = true;
                inside.growUp();
            }
        }
    };
    inside.growUp = function() {
        if ((inside.isPushing === true) && (inside.isPushUp)) {
            var pos = inside.level.getPosition();
            var toPos = new pc.Vec3(inside.pushTo.x, inside.pushTo.y, inside.pushTo.z);
            var toStep = new pc.Vec3(inside.pushStep.x, inside.pushStep.y, inside.pushStep.z);

            if (((toPos.x < 0) && (0 > pos.x) || (toPos.x > 0) && (0 < pos.x)) ||
                ((toPos.y < 0) && (0 > pos.y) || (toPos.y > 0) && (0 < pos.y)) ||
                ((toPos.z < 0) && (0 > pos.z) || (toPos.z > 0) && (0 < pos.z))) {

                var toStep = new pc.Vec3(inside.pushStep.x, inside.pushStep.y, inside.pushStep.z);
                if (toPos.x < 0)
                    toStep.x *= -1;
                if (toPos.y < 0)
                    toStep.y *= -1;
                if (toPos.z < 0)
                    toStep.z *= -1;

                inside.level.setPosition(pos.x - toStep.x, pos.y - toStep.y, pos.z - toStep.z);

                setTimeout(inside.growUp, inside.pushPeriod);
            } else {
                inside.level.setPosition(0, 0, 0);
                inside.isPushUp = false;
                inside.isPushing = false;

                if (self.player != null) {
                    self.player.locker = false;
                }
            }
        }
    };

    inside.lightOn = function() {
        if  (!inside.isDisplaying) {
            inside.display3d();
        }
        if (!inside.isDisplayed) {
            setTimeout(inside.lightOn, 500);
            return;
        }

        var light = new inside.pc.Entity();
        light.addComponent("light", {
            type: "directional",
            color: new inside.pc.Color(1, 1, 1),
//            castShadows: true,
//            shadowBias: 1.05,
//            normalOffsetBias: 0.05,
//            shadowResolution: 1024,
            intensity: 0.4
        });
        light.setEulerAngles(-30, 0, 30);
        inside.ground.addChild(light);

        light = new inside.pc.Entity();
        light.addComponent("light", {
            type: "directional",
            color: new inside.pc.Color(1, 1, 1),
            intensity: 0.2
        });
        light.setEulerAngles(30, 0, -30);
        inside.ground.addChild(light);

        light = new inside.pc.Entity();
        light.addComponent("light", {
            type: "spot",
            color: new inside.pc.Color(1, 1, 1),
            outerConeAngle: 60,
            innerConeAngle: 50,
            range: inside.sizeY * inside.sizeY * inside.sizeY,
            intensity: 1,
            castShadows: true,
            shadowBias: 0.005,
            normalOffsetBias: 0.01,
            shadowResolution: 2048
        });
        light.translate(inside.sizeX * 2 - 2, inside.sizeY * 2, inside.sizeY * 2 - 2);
        inside.ground.addChild(light);

        light = new inside.pc.Entity();
        light.addComponent("light", {
            type: "point",
            color: new inside.pc.Color(1, 1, 1),
            intensity: 1.0,
            range: inside.sizeY * 2
        });
        light.translate(inside.sizeX - 2, 5, inside.sizeY - 2);
        inside.ground.addChild(light);

        light = new inside.pc.Entity();
        light.addComponent("light", {
            type: "point",
            color: new inside.pc.Color(1.2, 1, 1),
            intensity: 1.0,
            range: inside.sizeY * 2
        });
        light.translate(inside.sizeX * 3, 5, inside.sizeY * 3);
        inside.ground.addChild(light);

        light = new inside.pc.Entity();
        light.addComponent("light", {
            type: "point",
            color: new inside.pc.Color(1, 1.2, 1),
            intensity: 1.0,
            range: inside.sizeY * 2
        });
        light.translate(inside.sizeX - 2, 5, inside.sizeY * 3);
        inside.ground.addChild(light);

        light = new inside.pc.Entity();
        light.addComponent("light", {
            type: "point",
            color: new inside.pc.Color(1, 1, 1.2),
            intensity: 1.0,
            range: inside.sizeY * 2
        });
        light.translate(inside.sizeX * 3, 5, inside.sizeY - 2);
        inside.ground.addChild(light);
    };



    //-- Public methods
    self.initMap = function(itemId) {
        inside.itemMap = document.getElementById(itemId);
    };
    self.initInfo = function(itemId) {
        inside.itemInfo = document.getElementById(itemId);
    };
    self.initLog = function(itemId) {
        inside.itemLog = document.getElementById(itemId);
    };

    self.showMap = function() {
        if (inside.itemMap != null) {
            inside.itemMap.textContent = self.getText();
        }
    };
    self.info = function(msg) {
        if (inside.itemInfo != null) {
            inside.itemInfo.textContent = msg;
        }
    };
    self.log = function(msg) {
        if (inside.itemLog != null) {
            msg = inside.itemLog.textContent + msg + "\n";
            if (msg.length > 400) {
                msg = msg.substr(msg.length - 400);
            }
            inside.itemLog.textContent = msg;
        } else {
            console.log(msg);
        }
    };

    self.addPicture = function(url, sizeX, sizeY) {
        ++inside.loadMaterials;
        inside.app.assets.loadFromUrl(url, "texture", function (err, asset) {
            var material = new inside.pc.StandardMaterial();
            material.diffuseMap = asset.resource;
            material.update();
            inside.materialPictures.push({
                material: material,
                sizeX: sizeX,
                sizeY: sizeY
            });
            --inside.loadMaterials;
        });
    };

    self.setWallColor = function(red, green, blue, alpha) {
        var color = new pc.Color();
        if (green == null) {
            color.fromString(red);
        } else {
            color.set(red, green, blue, alpha);
        }
        var material = new pc.PhongMaterial();
        material.diffuse = color;
        material.update();
        inside.materialWall = material;
    };
    self.setFloorColor = function(red, green, blue, alpha) {
        var color = new pc.Color();
        if (green == null) {
            color.fromString(red);
        } else {
            color.set(red, green, blue, alpha);
        }
        var material = new pc.PhongMaterial();
        material.diffuse = color;
        material.update();
        inside.materialFloor = material;
    };

    self.setWallMaterial = function(url) {
        ++inside.loadMaterials;
        inside.app.assets.loadFromUrl(url, "texture", function (err, asset) {
            var material = new inside.pc.StandardMaterial();
            material.diffuseMap = asset.resource;
            material.update();
            inside.materialWall = material;
            --inside.loadMaterials;
        });
        // inside.app.assets.loadFromUrl("assets/wall/6076297/wall.json", "material", function (err, asset) {
        //     console.log(asset);
        //     console.log(asset.data);
        //     console.log(asset.resource);
        //     // inside.wallMaterial = asset;
        //     // inside.wallMaterial.init(asset.data);
        //     // inside.wallMaterial.update();
        //     --inside.loadMaterials;
        // });
    };
    self.setFloorMaterial = function(url) {
        ++inside.loadMaterials;
        inside.app.assets.loadFromUrl(url, "texture", function (err, asset) {
            var material = new inside.pc.StandardMaterial();
            material.diffuseMap = asset.resource;
            material.update();
            inside.materialFloor = material;
            --inside.loadMaterials;
        });
    };

    self.getText = function() {
        if (!inside.isGenerated) {
            self.log("Maze not generated");
            return '';
        }
        var text = [];
        var playerPos = null;
        if (self.player != null)
            playerPos = self.player.getPosition();

        for (var j = 0; j < inside.sizeY * 2 + 1; j++) {
            var line= [];
            if (0 === j%2)
                for (var k = 0; k < inside.sizeX * 4 + 1; k++)
                    if (0 === k % 4)
                        line[k] = '+';
                    else if (j > 0 && inside.linesV[j/2-1][Math.floor(k/4)])
                        line[k] = ' ';
                    else
                        line[k] = '-';
            else
                for (var k = 0; k < inside.sizeX * 4 + 1; k++)
                    if (0 === k % 4)
                        if (k > 0 && inside.linesH[(j-1)/2][k/4-1])
                            line[k]= ' ';
                        else
                            line[k]= '|';
                    else
                        line[k]= ' ';
            if (Math.round(inside.winZ) / 2 + 1 === j)
                line[Math.round(inside.winX) + 2] = 'X';
            if (playerPos != null) {
                if (Math.round(playerPos.z / 2 + 1) === j)
                    line[Math.round(playerPos.x + 2)] = '*';
            }
            text.push(line.join('')+'\r\n');
        }
        return text.join('');
    };


    self.createPlayer = function(name, url) {
        if  (!inside.isDisplaying) {
            inside.display3d();
        }
        if (!inside.isDisplayed) {
            setTimeout(function(){
                self.createPlayer(name, url);
            }, 500);
            return;
        }

        self.player = new inside.pc.Entity();
        self.player.maze = self;
        self.player.locker = true;
        self.player.addComponent("script");
        inside.app.assets.loadFromUrl(url, "script", function (err, asset) {
            self.player.script.create(name, {
            });
            self.camera = self.player.script[name].camera;
        });
        self.player.rotate(0, 180, 0);

        inside.ground.addChild(self.player);
    };

    self.createPrize = function(name, url) {
        if  (!inside.isDisplaying) {
            inside.display3d();
        }
        if (!inside.isDisplayed) {
            setTimeout(function() {
                self.createPrize(name, url);
            }, 500);
            return;
        }

        self.prize = new inside.pc.Entity();
        self.prize.maze = self;
        self.prize.addComponent("script");
        inside.app.assets.loadFromUrl(url, "script", function (err, asset) {
            self.prize.script.create(name, {
                attributes: {
                    winX: inside.winX,
                    winY: inside.winY,
                    winZ: inside.winZ,
                    soundDist: inside.sizeY * 4
                }
            });
        });
        inside.ground.addChild(self.prize);
    };

    self.movePrize = function() {
        var oldX = (inside.winX > inside.sizeX) ? 1 : 0;
        var oldZ = (inside.winZ > inside.sizeY) ? 1 : 0;
        var newX, newZ;

        do {
            newX = Math.round(Math.random()*100) % 2;
            newZ = Math.round(Math.random()*100) % 2;
        } while ((oldX == newX) && (oldZ == newZ));

        inside.winX = (newX > 0) ? ((inside.sizeX - 1) * 4) : 0;
        inside.winZ = (newZ > 0) ? ((inside.sizeY - 1) * 4) : 0;

        self.prize.moveTo(inside.winX, inside.winY, inside.winZ);

        self.refresh();

        self.showMap();
    };

    self.refresh = function() {
        if (self.player != null) {
            self.player.locker = true;
        }

        inside.isPushing = true;
        if (inside.isGenerated) {
            inside.isPushUp = false;
            inside.growDown();
        } else {
            inside.level.setPosition(inside.pushTo);
            inside.generate();
            inside.makeWalls();

            inside.isPushUp = true;
            inside.growUp();
        }
    };

    inside.init();

    return self;
}
