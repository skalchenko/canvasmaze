## How to build

* Ensure you have [nodejs](https://nodejs.org) installed
* Ensure you have [Java](https://java.com/en/download/) installed.

The first time you build you will be asked to install dependencies using `npm`. e.g.

    npm install fs-extra
    npm install google-closure-compiler
    npm install preprocessor

Then, to execute a build of the engine to the build/output folder, do:

    cd build
    node build.js

See the built in help for more build instructions

    node build.js -h

Pre-built versions of the engine are also available.

Latest development release:

* https://code.playcanvas.com/playcanvas-latest.js
* https://code.playcanvas.com/playcanvas-latest.min.js

Latest stable release:

* https://code.playcanvas.com/playcanvas-stable.js
* https://code.playcanvas.com/playcanvas-stable.min.js

Specific engine versions:

* https://code.playcanvas.com/playcanvas-0.181.11.js
* https://code.playcanvas.com/playcanvas-0.181.11.min.js

## Documentation

Full documentation available on the [PlayCanvas Developer](http://developer.playcanvas.com) site including [API reference](http://developer.playcanvas.com/en/api/)

## Releases

A full list of Releases and Release Notes is available [here](https://github.com/playcanvas/engine/releases).

## How to get models?

To convert any models created using a 3D modelling package see [this page](http://developer.playcanvas.com/en/engine/) in the developer documentation.

## Getting Help

[**Forums**](http://forum.playcanvas.com) - Use the forum to ask/answer questions about Engine, Editor or generally PlayCanvas

## PlayCanvas Platform

The PlayCanvas Engine is an open source game engine which you can use to create games or render 3D in the browser. In addition to the engine we also make the [PlayCanvas development platform](https://playcanvas.com/) which features an Visual Editor, asset management, hosting and publishing services.

## License

The PlayCanvas Engine is released under the [MIT](http://opensource.org/licenses/MIT) license. See LICENSE file.
